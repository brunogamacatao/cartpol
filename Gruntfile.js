module.exports = function(grunt) {
	grunt.initConfig({

        // Connect definitions
        connect: {
            demo: {
                options: {
                    keepalive: true
                }
            }
        },

		// Lint definitions
		jshint: {
			options: {
				jshintrc: '.jshintrc'
			},
			files: {
				src: [
					'scripts/main.js',
					'scripts/utils/toolkit.js',
					'scripts/utils/configuraProjeto.js',
					'scripts/utils/dataBase.js',
					'scripts/helpers/helpers.js',
					'scripts/entidades/entidades.js',
					'scripts/controllers/login.js',
					'scripts/controllers/principal.js',
					'scripts/controllers/bo.js'
				]
			}
		}
	});

	// These plugins provide necessary tasks
	grunt.loadNpmTasks('grunt-contrib-connect');
	grunt.loadNpmTasks('grunt-contrib-jshint');

	// By server, lint and run all tests
	grunt.registerTask('server', ['connect']);
	grunt.registerTask('lint', ['jshint']);
};
