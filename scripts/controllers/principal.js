/*
 * Laboratório de Tecnologia da Informação - LTI - CESED - 2014
 * Todos os direitos reservados
 *
 * Software desenvolvido para a Polícia Civil de Campina Grande - PB
 *
 * Script com as funcoes que implementam o modulo controlador da página 
 * principal do sistema.
 */
 
cartpol.controlador.principal = {
  exibirForm: function() {
    toolkit.fillElementWithTemplate("main_content", "main_page", {}, cartpol.controlador.principal.funcoesForm);
  },
  funcoesForm: function() {
    $(".botao-novo-bo").click(cartpol.controlador.bo.exibirForm);
    //$(".botao-lista-bo").click(cartpol.exibePaginaListaBOs);
    toolkit.fillElementWithTemplate("barra-de-ferramentas", "barra_de_ferramentas", {}, cartpol.controlador.principal.funcoesBarraDeFerramentas);
  },
  funcoesBarraDeFerramentas: function() {
    $("#sair_btn").click(cartpol.controlador.login.exibirForm);
    $(".botao-novo-bo").click(cartpol.controlador.bo.exibirForm);
    //$(".botao-lista-bo").click(cartpol.exibePaginaListaBOs);
  }
};