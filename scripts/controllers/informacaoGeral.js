/*
 * Laboratório de Tecnologia da Informação - LTI - CESED - 2014
 * Todos os direitos reservados
 *
 * Software desenvolvido para a Polícia Civil de Campina Grande - PB
 *
 * Script com as funcoes que implementam o modulo controlador da página 
 * de informações gerais.
 */

cartpol.controlador.informacaoGeral = {
  exibirForm: function() {
    toolkit.fillElementWithTemplate("main_content", "informacao_geral/form", {}, cartpol.controlador.informacaoGeral.funcoesForm);
  },
  funcoesForm: function() {
    $("#informacao_geral_form").submit(function(evt) {
      evt.preventDefault();     
      var dados = {
        superintendencia: $('#superintendencia option:selected').val(),
        dspc: $('#dspc option:selected').val(),
        delegacia: $('#delegacia option:selected').val(),
        delegado: $('#delegado option:selected').val()
      };
      cartpol.globals.informacaoGeral = new cartpol.entidades.InformacaoGeral(dados);
      cartpol.controlador.principal.exibirForm();
      return false;
    });
  }

};