/*
 * Laboratório de Tecnologia da Informação - LTI - CESED - 2014
 * Todos os direitos reservados
 *
 * Software desenvolvido para a Polícia Civil de Campina Grande - PB
 *
 * Script com as funcoes que implementam o modulo controlador do login.
 */
 
cartpol.controlador.login = {
  exibirForm: function() {
    $("#barra-de-ferramentas").html("");
    toolkit.fillElementWithTemplate("main_content", "login/form", {}, cartpol.controlador.login.funcoesForm);
   
  },
  validarLoginOnline: function(){
    //TODO
  },
  validarLoginOffline: function(){
    var usuario = {
      matricula: $("#inputMatricula").val(),
      senha: $.md5($("#inputSenha").val())
    }
    
    var resultado = cartpol.dataBase.recuperarUsuarioPorMatriculaSenha(usuario);
    if (resultado){
      cartpol.controlador.informacaoGeral.exibirForm();
      //Salvo o usuario no LocalStorage.
      if(typeof(Storage) !== "undefined") {
        localStorage.usuario = resultado;
      }
    }
    else {
      //Mostro DIV e seto o erro.
      $("#msg").show();
      $("#msg").html("Matrícula/Senha inválidos");
    }
    
  },
  funcoesForm: function() {
    require(['md5'], function(){
      $("#login_form").submit(function(evt) {
        evt.preventDefault();
        // cartpol.controlador.principal.exibirForm();

        //Força a checkagem
        Offline.check();

        if (Offline.state == "up"){
          //cartpol.controlador.login.validarLoginOnline();
          cartpol.controlador.login.validarLoginOffline();
        }
        else{
          cartpol.controlador.login.validarLoginOffline();
        }

        return false;
      });
    });
  }
};